#!/usr/bin/env bash
set -eux
cd "$(dirname "$0")" || exit $?

source download.sh

echo "Installing .Xresources"
cp -f "${PWD}/.Xresources" "${HOME}/.Xresources"

echo "Installing scripts to .local/bin"
cp -f "${PWD}"/.local/bin/* "${HOME}/.local/bin"

echo "Installing xrdb configs and color schemes"
cp -rf "${PWD}/.config/xrdb" "${HOME}/.config/xrdb"
#cp "${PWD}/.config/xrdb/main.xrdb" "${HOME}/.Xresources"

echo "Installing xrdb-replace and config"
download "https://raw.githubusercontent.com/palmdrop/xrdb-replace/main/xrdb-replace" "${HOME}/.local/bin/xrdb-replace"
chmod +x "${HOME}/.local/bin/xrdb-replace"
cp -rf "${PWD}/.config/xrdb-replace" "${HOME}/.config/xrdb-replace"

echo "Installing bspwm config"
cp -rf "${PWD}/.config/bspwm" "${HOME}/.config/bspwm"

echo "Installing polybar config and scripts"
mkdir -p "${HOME}/.config/polybar/scripts"
cp -f "${PWD}/.config/polybar/config" "${HOME}/.config/polybar/config"
cp -rf "${PWD}"/.config/polybar/scripts/* "${HOME}/.config/polybar/scripts/"

echo "Installing rofi config"
mkdir -p "${HOME}/.config/rofi"
cp -f "${PWD}"/.config/rofi/* "${HOME}/.config/rofi/"

echo "Installing sxhkd config"
mkdir -p "${HOME}/.config/sxhkd"
cp -f "${PWD}/.config/sxhkd/sxhkdrc" "${HOME}/.config/sxhkd/sxhkdrc"

# Common
cd ../common

echo "Installing alacritty config"
cp -rf "${PWD}/alacritty" "${HOME}/.config/alacritty"

echo "Installing neovim config"
cp -rf "${PWD}/nvim" "${HOME}/.config/nvim"

echo "Installing eslint configs"
cp -f "${PWD}/.eslintignore" "${HOME}/.eslintignore"
cp -f "${PWD}/.eslintrc.js" "${HOME}/.eslintrc.js"

echo "Installing .prettierrc"
cp -f "${PWD}/.prettierrc" "${HOME}/.prettierrc"

echo "Installing .XCompose"
cp -f "${PWD}/.XCompose" "${HOME}/.XCompose"

echo "Installing .zshrc"
cp -f "${PWD}/.zshrc" "${HOME}/.zshrc"

# Distro agnostic tools to be compiled
echo "-- Compiling xgetres --"
echo "Cloning..."
git clone https://github.com/tamirzb/xgetres.git /tmp/xgetres
cd /tmp/xgetres

echo "Compiling..."
make

echo "Installing..."
sudo make install

echo "-- Compiling xmousepasteblock --"
echo "Cloning..."
git clone https://github.com/milaq/XMousePasteBlock.git /tmp/xmpb
cd /tmp/xmpb

echo "Compiling..."
make

echo "Installing..."
sudo make install
