#!/bin/sh

# this only exists to stop socket deadlocking (hopefully)

if ! pgrep -x cmus ; then
  echo "cmus-remote: cmus is not running"
  exit 1
else
  cmus-remote -Q
fi
