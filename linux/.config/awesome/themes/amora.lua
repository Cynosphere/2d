---------------------------
-- Default awesome theme --
---------------------------

local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local gfs = require("gears.filesystem")
local themes_path = gfs.get_themes_dir()

local colors = {
    background = "#302838",
    foreground = "#dedbeb",
    secondary  = "#2a2331",
    tertiary   = "#231d29",
    accent     = "#e83f80",

    color0 = "#533f61",
    color1 = "#e83f80",
    color2 = "#a2baa8",
    color3 = "#eacac0",
    color4 = "#9985d1",
    color5 = "#e68ac1",
    color6 = "#aabae7",
    color7 = "#dedbeb",

    color8  = "#634e75",
    color9  = "#f55d8f",
    color10 = "#bfd1c3",
    color11 = "#f0ddd8",
    color12 = "#b4a4de",
    color13 = "#edabd2",
    color14 = "#c4d1f5",
    color15 = "#edebf7",
}

local theme = dofile(themes_path .. "default/theme.lua")

theme.__colors = colors

theme.bg_normal     = colors.secondary
theme.bg_focus      = colors.background
theme.bg_urgent     = colors.color0
theme.bg_minimize   = colors.tertiary
theme.bg_systray    = colors.background

theme.fg_normal     = colors.color7
theme.fg_focus      = colors.foreground
theme.fg_urgent     = colors.color1
theme.fg_minimize   = colors.color0

theme.border_normal = theme.bg_normal
theme.border_focus  = colors.accent
theme.border_marked = colors.color2

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
--theme.taglist_bg_focus = "#ff0000"

theme.taglist_bg_focus    = colors.background
theme.taglist_bg_urgent   = theme.taglist_bg_focus
theme.taglist_bg_occupied = theme.taglist_bg_focus
theme.taglist_bg_empty    = theme.taglist_bg_focus
theme.taglist_bg_volatile = theme.taglist_bg_focus

theme.taglist_fg_focus    = colors.color5
theme.taglist_fg_urgent   = colors.color1
theme.taglist_fg_occupied = colors.color2
theme.taglist_fg_empty    = colors.foreground
theme.taglist_fg_volatile = colors.color3

theme.tasklist_fg_focus  = colors.color5
theme.tasklist_fg_urgent = colors.color1

theme.wibar_bg = colors.background
theme.wibar_fg = colors.foreground

theme.layout_fg = colors.foreground

-- bar items
theme.widget_clock = colors.color6
theme.widget_volume = colors.color4
theme.widget_volume_muted = colors.color9
theme.widget_cpu = colors.color10
theme.widget_gpu = colors.color14
theme.widget_memory = colors.color11
theme.widget_music = colors.color13
theme.widget_packages = colors.color4
theme.widget_net_up = colors.color1
theme.widget_net_down = colors.color2

-- Generate taglist squares:
--[[local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)--]]
theme.taglist_squares_sel = nil
theme.taglist_squares_unsel = nil

-- Variables set for theming notifications:
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]

theme.notification_bg = colors.background
theme.notification_fg = colors.foreground
theme.notification_border_color = colors.color5

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_bg_normal = colors.background
theme.menu_bg_focus  = colors.accent

theme.menu_fg_normal = colors.foreground
theme.menu_fg_focus  = "#ffffff"

theme.menu_border_color = colors.background

-- Generate Awesome icon:
theme.awesome_icon = theme_assets.awesome_icon(
    16, colors.background, colors.accent
)

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

theme.win9x_main = colors.background
theme.win9x_bright = colors.color8
theme.win9x_dark = colors.color0
theme.win9x_outer_bright = colors.secondary
theme.win9x_outer_dark = colors.tertiary
theme.win9x_focus = colors.accent
theme.win9x_unfocus = colors.color8

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
