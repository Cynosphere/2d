-- luacheck: globals awesome root globalkeys client screen debug_print

-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Themehandling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local lain = require("lain")
local freedesktop = require("freedesktop")
local cyclefocus = require("cyclefocus")
local hotkeys_popup = require("awful.hotkeys_popup")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

-- {{{ Error handling
-- Checkif awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({
        preset = naughty.config.presets.critical,
        title = "Oops, there were errors during startup!",
        text = awesome.startup_errors
    })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end

        in_error = true
        naughty.notify({
            preset = naughty.config.presets.critical,
            title = "Oops, an error happened!",
            text = tostring(err)
        })
        in_error = false
    end)
end
-- }}}

-- {{{ helpers
function debug_print(val)
    naughty.notify({
        title = "debug",
        text = tostring(val),
        preset = naughty.config.presets.critical,
    })
end
-- }}}

-- {{{ Variable definitions
local HOME = os.getenv("HOME")
local CONFIG_DIR = gears.filesystem.get_xdg_config_home() .. "awesome/"
local ICON_DIR = CONFIG_DIR .. "icons"

local config = require("config")
local terminal = config.terminal or "xterm"
local terminal_separator = config.terminal_separator or "-e"
local editor = config.editor or os.getenv("EDITOR") or "nano"
local editor_cmd = terminal .. " " .. terminal_separator .. " " .. editor
local modkey = config.modkey or "Mod4"
local altkey = config.altkey or "Mod1"

cyclefocus.move_mouse_pointer = false
cyclefocus.cycle_filters = {
    function() return true end,
}

-- Themes define colours, icons, font and wallpapers.
do
    local theme = require("themes/".. config.theme)

    require("icons")(ICON_DIR, theme)

    if config.font then
        theme.font = config.font
    end
    if config.gap then
        theme.useless_gap = config.gap
    end
    if config.border_width then
        theme.border_width              = config.border_width
        theme.menu_border_width         = config.border_width
        theme.notification_border_width = config.border_width
    end
    if config.icon_theme then
        theme.icon_theme = config.icon_theme
    end

    theme.menu_height = 16
    theme.menu_width = 128

    theme.wibar_height = 20

    theme = beautiful.theme_assets.recolor_titlebar(theme, theme.win9x_unfocus, "normal")
    theme = beautiful.theme_assets.recolor_titlebar(theme, theme.fg_focus, "focus")
    theme = beautiful.theme_assets.recolor_layout(theme, theme.layout_fg)

    local lain_layouts = {
        "termfair",
        "centerfair",
        "cascade",
        "cascadetile",
        "centerwork",
        "centerworkh"
    }
    for _, key in ipairs(lain_layouts) do
        local image = gears.surface.duplicate_surface(theme["layout_" .. key])
        theme["layout_" .. key] = gears.color.recolor_image(image, theme.layout_fg)
    end

    do
        local image = gears.surface.duplicate_surface(theme.menu_submenu_icon)
        theme.menu_submenu_icon = gears.color.recolor_image(image, theme.menu_fg_normal)
    end

    local bar_icons = {
        clock = {
            path = "clock",
            color = theme.widget_clock,
        },
        volume = {
            path = "volume",
            color = theme.widget_volume,
        },
        volume_muted = {
            path = "volume_muted",
            color = theme.widget_volume_muted,
        },
        memory = {
            path = "memory",
            color = theme.widget_memory,
        },
        gpu = {
            path = "gpu",
            color = theme.widget_gpu,
        },
        gpu_temp = {
            path = "temp",
            color = theme.widget_gpu,
        },
        cpu = {
            path = "cpu",
            color = theme.widget_cpu,
        },
        cpu_temp = {
            path = "temp",
            color = theme.widget_cpu,
        },
        music = {
            path = "music",
            color = theme.widget_music,
        },
        packages = {
            path = "packages",
            color = theme.widget_packages,
        },
        net_up = {
            path = "net_up",
            color = theme.widget_net_up,
        },
        net_down = {
            path = "net_down",
            color = theme.widget_net_down,
        },
    }

    for key, icon in pairs(bar_icons) do
        local image = gears.surface.duplicate_surface(ICON_DIR .. "/bar/" .. icon.path .. ".png")
        theme["bar_" .. key] = gears.color.recolor_image(image, icon.color or theme.wibar_fg)
    end

    theme.tasklist_floating = "~"

    if not beautiful.init(theme) then
        beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")
    end
end
if config.wallpaper then
    beautiful.wallpaper = config.wallpaper
end
-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {}
for _, layout in ipairs(config.layouts) do
    local layoutRef = awful.layout.suit
    if layout:find("^lain:") then
        layoutRef = lain.layout
        layout = layout:gsub("^lain:","")
    end

    if layout:find("%.") then
        local perStart, perEnd = layout:find("%.")
        local first = layout:sub(1, perStart - 1)
        local second = layout:sub(perEnd + 1, #layout)

        table.insert(awful.layout.layouts, layoutRef[first][second])
    else
        table.insert(awful.layout.layouts, layoutRef[layout])
    end
end
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
local awesomemenu = {
    { "hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
    { "manual", terminal .. " " .. terminal_separator .. " man awesome" },
    { "edit config", editor_cmd .. " " .. awesome.conffile },
    { "restart", awesome.restart },
    { "quit", function() awesome.quit() end },
}

local mainmenu = freedesktop.menu.build({
    before = {
        { "awesome", awesomemenu, beautiful.awesome_icon },
    },
    after = {
        { "open terminal", terminal },
    },
})

local launcher = awful.widget.launcher({
    image = beautiful.awesome_icon,
    menu = mainmenu
})

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, false)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

-- {{{ Bar
local markup = lain.util.markup

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
    awful.button({}, 1, function(t)
        t:view_only()
    end),
    awful.button({modkey}, 1, function(t)
        if client.focus then
            client.focus:move_to_tag(t)
        end
    end),
    awful.button({}, 3, awful.tag.viewtoggle),
    awful.button({modkey}, 3, function(t)
        if client.focus then
            client.focus:toggle_tag(t)
        end
    end),
    awful.button({}, 4, function(t)
        awful.tag.viewnext(t.screen)
    end),
    awful.button({}, 5, function(t)
        awful.tag.viewprev(t.screen)
    end)
)

local tasklist_buttons = gears.table.join(
    awful.button({}, 1, function(c)
        if c == client.focus then
            c.minimized = true
        else
            c:emit_signal(
                "request::activate",
                "tasklist",
                {raise = true}
            )
        end
    end),
    awful.button({}, 3, function()
        awful.menu.client_list({
            theme = {
                width = 256
            }
        })
    end),
    awful.button({}, 4, function()
        awful.client.focus.byidx(1)
    end),
    awful.button({}, 5, function()
        awful.client.focus.byidx(-1)
    end)
)

local layout_buttons = gears.table.join(
    awful.button({}, 1, function()
        awful.layout.inc(1)
    end),
    awful.button({}, 3, function()
        awful.layout.inc(-1)
    end),
    awful.button({}, 4, function()
        awful.layout.inc(1)
    end),
    awful.button({}, 5, function()
        awful.layout.inc(-1)
    end)
)

local function makeBarIcon(icon)
    local img = wibox.widget.imagebox(beautiful["bar_" .. icon], false)
    img.forced_height = 18
    img.forced_width = 18
    return img
end

-- Widgets
local clock_icon = makeBarIcon("clock")
local uclock = wibox.widget.textclock(markup(beautiful.widget_clock or beautiful.wibar_fg, "(UTC: %H) "), 1, "UTC")
local clock = wibox.widget.textclock(markup(beautiful.widget_clock or beautiful.wibar_fg, "%H:%M:%S"), 1)
lain.widget.cal({
    attach_to = {clock},
    week_start = 1,
    week_number = "left",
    followtag = true,
    icons = "",
    notification_preset = {
        font = config.font,
        fg = beautiful.notification_fg,
        bg = beautiful.notification_bg,
    },
})

--[[local volume_icon = makeBarIcon("volume")
local volume = lain.widget.alsa({
    settings = function()
        if volume_now.status == "off" then
            volume_now.level = "MUTE"
        else
            volume_now.level = volume_now.level .. "%"
        end

        volume_icon.image = volume_now.status == "off" and beautiful.bar_volume_muted or beautiful.bar_volume

        widget:set_markup(markup(volume_now.status == "off" and beautiful.widget_volume_muted or beautiful.widget_volume, volume_now.level))
    end,
})
local volume_buttons =  awful.util.table.join(
    awful.button({}, 1, function() -- left click
        os.execute(string.format("%s set %s toggle", volume.cmd, volume.togglechannel or volume.channel))
        volume.update()
    end),
    awful.button({}, 4, function() -- scroll up
        os.execute(string.format("%s set %s 1%%+", volume.cmd, volume.channel))
        volume.update()
    end),
    awful.button({}, 5, function() -- scroll down
        os.execute(string.format("%s set %s 1%%-", volume.cmd, volume.channel))
        volume.update()
    end)
)
volume.widget:buttons(volume_buttons)
volume_icon:buttons(volume_buttons)--]]

local memory_icon = makeBarIcon("memory")
local memory = lain.widget.mem({
    settings = function()
        widget:set_markup(markup(beautiful.widget_memory or beautiful.wibar_fg, mem_now.used .. " MB, " .. mem_now.swapused .. " MB"))
    end
})

local cpu_icon = makeBarIcon("cpu")
local cpu = lain.widget.cpu({
    settings = function()
        widget:set_markup(markup(beautiful.widget_cpu or beautiful.wibar_fg, cpu_now.usage .. "%"))
    end
})

local cpu_temp_icon = makeBarIcon("cpu_temp")
local cpu_temp = wibox.widget.textbox()
awful.widget.watch(
    'bash -c "cat /sys/class/hwmon/hwmon1/temp1_input"',
    1,
    function(widget, stdout, stderr)
        widget:set_markup(markup(beautiful.widget_cpu or beautiful.wibar_fg, math.floor(tonumber(stdout) / 1000) .. "°C"))
    end,
    cpu_temp
)

local gpu_icon = makeBarIcon("gpu")
local gpu = wibox.widget.textbox()
awful.widget.watch(
    'bash -c "radeontop -d - -l 1"',
    1,
    function(widget, stdout, stderr)
        local usage = math.floor(tonumber(stdout:match("gpu (.-)%%")))
        local vram = math.floor(tonumber(stdout:match("vram .- (.-)mb")))

        widget:set_markup(markup(beautiful.widget_gpu or beautiful.wibar_fg, usage .. "%, " .. vram .. " MB"))
    end,
    gpu
)

local gpu_temp_icon = makeBarIcon("gpu_temp")
local gpu_temp = wibox.widget.textbox()
awful.widget.watch(
    'bash -c "cat /sys/class/hwmon/hwmon0/temp1_input"',
    1,
    function(widget, stdout, stderr)
        widget:set_markup(markup(beautiful.widget_gpu or beautiful.wibar_fg, math.floor(tonumber(stdout) / 1000) .. "°C"))
    end,
    gpu_temp
)

local packages_icon = makeBarIcon("packages")
local packages = wibox.widget.textbox()
local packages_wrapper = wibox.widget({
    {
        packages_icon,
        packages,
        layout = wibox.layout.fixed.horizontal,
    },
    top = 2,
    bottom = 2,
    left = 4,
    right = 4,
    layout = wibox.container.margin,
})

awful.widget.watch(
    'bash -c "xbps-install -Mun | wc -l"',
    900,
    function(widget, stdout, stderr)
        local count = tonumber(stdout)
        if count == 0 then
            packages_wrapper.visible = false

            widget:set_markup("")
        else
            packages_wrapper.visible = true
            widget:set_markup(markup(beautiful.widget_packages or beautiful.wibar_fg, count .. " pkg" .. (count > 1 and "s" or "")))
        end
    end,
    packages
)

--[[local function gm_round(num, idp)
    local mult = 10 ^ (idp or 0)
    return math.floor(num * mult + 0.5) / mult
end
local function format_size(size)
    size = tonumber(size)

    if size <= 0 then return "0" end
    if size < 1024 then return size .. " B" end
    if size < 1024 * 1024 then return gm_round(size / 1024, 2) .. " KB" end
    if size < 1024 * 1024 * 1024 then return gm_round( size / (1024 * 1024), 2) .. " MB" end

    return gm_round(size / (1024 * 1024 * 1024), 2) .. " GB"
end

local net_up_icon = makeBarIcon("net_up")
local net_down_icon = makeBarIcon("net_down")
local net_down = wibox.widget.textbox()
local net_up = lain.widget.net({
    units = 1,
    settings = function()
        widget:set_markup(markup(beautiful.widget_net_up or beautiful.wibar_fg, format_size(net_now.sent)))
        net_down:set_markup(markup(beautiful.widget_net_down or beautiful.wibar_fg, format_size(net_now.received)))
    end,
})--]]

local function format_time(time)
    local out = ""

    if time >= 3600 then
        out = out .. string.format("%02d:", math.floor(time / 3600))
    end

    out = out .. string.format("%02d:%02d", math.floor(time % 3600 / 60), math.floor(time % 60))

    return out
end

local music_icon = makeBarIcon("music")
local music = wibox.widget.textbox()
local music_wrapper = wibox.widget({
    {
        music_icon,
        music,
        layout = wibox.layout.fixed.horizontal,
    },
    top = 2,
    bottom = 2,
    left = 4,
    right = 4,
    layout = wibox.container.margin,
})

awful.widget.watch(
    'bash -c "~/.config/awesome/scripts/cmus-wrapper.sh"',
    1,
    function(widget, stdout, stderr, _, code)
        if code ~= 0 or stderr:match("^cmus-remote:") or stdout == "" then
            music_wrapper.visible = false

            widget:set_markup("")
        else
            music_wrapper.visible = true

            local nowplaying

            local status = stdout:match("status (.-)\n")
            local duration = tonumber(stdout:match("duration (.-)\n"))
            local position = tonumber(stdout:match("position (.-)\n"))

            if status == "stopped" then
                nowplaying = "[stopped]"
            else
                if duration == -1 and stdout:find("stream ") then
                    local stream = stdout:match("stream (.-)\n"):gsub("^%s*(.-)%s*$", "%1")

                    nowplaying = stream .. " [" .. format_time(position) .. "]"
                else
                    local artist = stdout:match("tag artist (.-)\n"):gsub("^%s*(.-)%s*$", "%1")
                    local title = stdout:match("tag title (.-)\n"):gsub("^%s*(.-)%s*$", "%1")

                    nowplaying = artist .. " - " .. title .. " [" .. format_time(position) .. "/" .. format_time(duration) .. "]"
                end

                if status == "paused" then
                    nowplaying = nowplaying .. " [" .. status .. "]"
                end
            end

            widget:set_markup(markup(beautiful.widget_music, gears.string.xml_escape(nowplaying)))
        end
    end,
    music
)

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- Each screen has its own tag table.
    awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.__promptbox = awful.widget.prompt()

    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.__layoutbox = awful.widget.layoutbox(s)
    s.__layoutbox:buttons(layout_buttons)

    -- Create a taglist widget
    s.__taglist = awful.widget.taglist({
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = taglist_buttons
    })

    -- Create a tasklist widget
    s.__tasklist = awful.widget.tasklist({
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons,
        widget_template = {
            {
                {
                    {
                        id = "icon_role",
                        widget = wibox.widget.imagebox,
                    },
                    margins = 2,
                    widget = wibox.container.margin,
                },
                {
                    {
                        id = "text_role",
                        widget = wibox.widget.textbox,
                    },
                    left = 2,
                    widget = wibox.container.margin,
                },
                layout = wibox.layout.fixed.horizontal
            },
            id = "background_role",
            widget = wibox.container.background,
        },
    })

    -- Create the wibox
    s.__bar = awful.wibar({
        position = "top",
        screen = s
    })

    -- Add widgets to the wibox
    s.__bar:setup({
        layout = wibox.layout.align.horizontal,

        -- Left widgets
        {
            layout = wibox.layout.fixed.horizontal,
            launcher,
            s.__taglist,
            {
                s.__layoutbox,
                margins = 2,
                layout = wibox.container.margin,
            },
            s.__promptbox,
        },

        -- Middle widgets
        s.__tasklist,

        -- Right widgets
        {
            music_wrapper,
            {
                {
                    cpu_icon,
                    cpu.widget,
                    layout = wibox.layout.fixed.horizontal,
                },
                top = 2,
                bottom = 2,
                left = 4,
                right = 4,
                layout = wibox.container.margin,
            },
            {
                {
                    cpu_temp_icon,
                    cpu_temp,
                    layout = wibox.layout.fixed.horizontal,
                },
                top = 2,
                bottom = 2,
                left = 4,
                right = 4,
                layout = wibox.container.margin,
            },
            {
                {
                    gpu_icon,
                    gpu,
                    layout = wibox.layout.fixed.horizontal,
                },
                top = 2,
                bottom = 2,
                left = 4,
                right = 4,
                layout = wibox.container.margin,
            },
            {
                {
                    gpu_temp_icon,
                    gpu_temp,
                    layout = wibox.layout.fixed.horizontal,
                },
                top = 2,
                bottom = 2,
                left = 4,
                right = 4,
                layout = wibox.container.margin,
            },
            {
                {
                    memory_icon,
                    memory.widget,
                    layout = wibox.layout.fixed.horizontal,
                },
                top = 2,
                bottom = 2,
                left = 4,
                right = 4,
                layout = wibox.container.margin,
            },
            --[[{
                {
                    net_down_icon,
                    net_down,
                    layout = wibox.layout.fixed.horizontal,
                },
                top = 2,
                bottom = 2,
                left = 4,
                right = 4,
                layout = wibox.container.margin,
            },
            {
                {
                    net_up_icon,
                    net_up.widget,
                    layout = wibox.layout.fixed.horizontal,
                },
                top = 2,
                bottom = 2,
                left = 4,
                right = 4,
                layout = wibox.container.margin,
            },--]]
            packages_wrapper,
            --[[{
                {
                    volume_icon,
                    volume.widget,
                    layout = wibox.layout.fixed.horizontal,
                },
                top = 2,
                bottom = 2,
                left = 4,
                right = 4,
                layout = wibox.container.margin,
            },--]]
            {
                {
                    clock_icon,
                    uclock,
                    clock,
                    layout = wibox.layout.fixed.horizontal,
                },
                top = 2,
                bottom = 2,
                left = 4,
                right = 4,
                layout = wibox.container.margin,
            },
            {
                wibox.widget.systray(),
                margins = 2,
                layout = wibox.container.margin,
            },
            layout = wibox.layout.fixed.horizontal,
        },
    })
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(gears.table.join(
    awful.button({}, 3, function()
        mainmenu:toggle()
    end),
    awful.button({}, 4, awful.tag.viewnext),
    awful.button({}, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings


globalkeys = gears.table.join(
    -- awesome
    awful.key(
        {modkey, altkey}, "r",
        awesome.restart,
        {
            description = "reload awesome",
            group = "awesome",
        }
    ),
    awful.key(
        {modkey, altkey}, "q",
        awesome.quit,
        {
            description = "quit awesome",
            group = "awesome",
        }
    ),
    awful.key(
        {modkey}, "s",
        hotkeys_popup.show_help,
        {
            description="show help",
            group = "awesome",
        }
    ),
    awful.key(
        {modkey}, "w",
        function()
            mainmenu:show()
        end,
        {
            description = "show main menu",
            group = "awesome",
        }
    ),
    awful.key(
        {modkey}, "x",
        function()
            awful.prompt.run({
                prompt       = "Run Lua code: ",
                textbox      = awful.screen.focused().__promptbox.widget,
                exe_callback = awful.util.eval,
                history_path = awful.util.get_cache_dir() .. "/history_eval"
            })
        end,
        {
            description = "lua execute prompt",
            group = "awesome",
        }
    ),

    -- launcher
    awful.key(
        {modkey}, "Return",
        function()
            awful.spawn(terminal)
        end,
        {
            description = "open a terminal",
            group = "launcher",
        }
    ),
    awful.key(
        {"Control", altkey}, "p",
        function()
            menubar.show()
        end,
        {
            description = "open launcher",
            group = "launcher",
        }
    ),
    awful.key(
        {modkey}, "r",
        function()
            awful.screen.focused().__promptbox:run()
        end,
        {
            description = "run prompt",
            group = "launcher",
        }
    ),
    awful.key(
        {}, "Print",
        function()
            awful.spawn.with_line_callback("screenie-min", {
                stdout = function(path)
                    awful.menu({
                        {
                            "Upload Image",
                            function()
                                awful.spawn('elixiremanager.sh "' .. path .. '"')
                            end,
                        },
                        {
                            "Copy Image",
                            function()
                                awful.spawn('xclip -selection clipboard -target image/png "' .. path .. '"')
                            end,
                        }
                    }):show()
                end,
            })
        end,
        {
            description = "run screenshot tool",
            group = "launcher",
        }
    ),

    -- client
    awful.key(
        {modkey}, "Up",
        function()
            awful.client.focus.byidx(1)
        end,
        {
            description = "focus next by index",
            group = "client",
        }
    ),
    awful.key(
        {modkey}, "Down",
        function()
            awful.client.focus.byidx(-1)
        end,
        {
            description = "focus previous by index",
            group = "client",
        }
    ),
    awful.key(
        {modkey, "Shift"}, "Up",
        function()
            awful.client.swap.byidx(1)
        end,
        {
            description = "swap with next client by index",
            group = "client",
        }
    ),
    awful.key(
        {modkey, "Shift"}, "Down",
        function()
            awful.client.swap.byidx(-1)
        end,
        {
            description = "swap with previous client by index",
            group = "client",
        }
    ),
    awful.key(
        {modkey}, "u",
        awful.client.urgent.jumpto,
        {
            description = "jump to urgent client",
            group = "client",
        }
    ),
    --[[awful.key(
        {altkey}, "Tab",
        function()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {
            description = "go back",
            group = "client",
        }
    ),--]]
    cyclefocus.key(
        {altkey}, "Tab"
    ),
    cyclefocus.key(
        {altkey, "Shift"}, "Tab"
    ),

    -- layout
    awful.key(
        {modkey}, "Right",
        function()
            awful.tag.incmwfact(0.05)
        end,
        {
            description = "increase master width factor",
            group = "layout",
        }
    ),
    awful.key(
        {modkey}, "Left",
        function()
            awful.tag.incmwfact(-0.05)
        end,
        {
            description = "decrease master width factor",
            group = "layout",
        }
    ),
    awful.key(
        {modkey, "Control"}, "Up",
        function()
            awful.tag.incnmaster(1, nil, true)
        end,
        {
            description = "increase the number of master clients",
            group = "layout",
        }
    ),
    awful.key(
        {modkey, "Control"}, "Down",
        function()
            awful.tag.incnmaster(-1, nil, true)
        end,
        {
            description = "decrease the number of master clients",
            group = "layout",
        }
    ),
    awful.key(
        {modkey, "Control"}, "Right",
        function()
            awful.tag.incncol(1, nil, true)
        end,
        {
            description = "increase the number of columns",
            group = "layout",
        }
    ),
    awful.key(
        {modkey, "Control"}, "Left",
        function()
            awful.tag.incncol(-1, nil, true)
        end,
        {
            description = "decrease the number of columns",
            group = "layout",
        }
    ),
    awful.key(
        {modkey}, "Tab",
        function()
            awful.layout.inc(1)
        end,
        {
            description = "select next layout",
            group = "layout",
        }
    ),
    awful.key(
        {modkey, "Shift"}, "Tab",
        function()
            awful.layout.inc(-1)
        end,
        {
            description = "select previous layout",
            group = "layout",
        }
    ),

    -- cmus
    awful.key(
        {}, "XF86AudioPlay",
        function()
            awful.spawn("cmus-remote -u")
        end,
        {
            description = "play/pause",
            group = "cmus",
        }
    ),
    awful.key(
        {}, "XF86AudioPrev",
        function()
            awful.spawn("cmus-remote -r")
        end,
        {
            description = "previous track",
            group = "cmus",
        }
    ),
    awful.key(
        {}, "XF86AudioNext",
        function()
            awful.spawn("cmus-remote -n")
        end,
        {
            description = "next track",
            group = "cmus",
        }
    )
)

local clientkeys = gears.table.join(
    awful.key(
        {modkey, "Shift"}, "f",
        function(c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {
            description = "toggle fullscreen",
            group = "client",
        }
    ),
    awful.key(
        {modkey}, "q",
        function(c)
            c:kill()
        end,
        {
            description = "close",
            group = "client",
        }
    ),
    awful.key(
        {modkey}, "f",
        awful.client.floating.toggle,
        {
            description = "toggle floating",
            group = "client",
        }
    ),
    awful.key(
        {modkey}, "t",
        function(c)
            c.sticky = not c.sticky
        end,
        {
            description = "toggle sticky",
            group = "client",
        }
    ),
    awful.key(
        {modkey, "Shift"}, "t",
        function(c)
            c.ontop = not c.ontop
        end,
        {
            description = "toggle keep on top",
            group = "client",
        }
    ),
    awful.key(
        {modkey}, "m",
        function(c)
            c.maximized = not c.maximized
            c:raise()
        end,
        {
            description = "(un)maximize",
            group = "client",
        }
    ),
    awful.key(
        {modkey, "Control"}, "m",
        function (c)
            c.maximized_vertical = not c.maximized_vertical
            c:raise()
        end,
        {
            description = "(un)maximize vertically",
            group = "client",
        }
    ),
    awful.key(
        {modkey, "Shift"}, "m",
        function(c)
            c.maximized_horizontal = not c.maximized_horizontal
            c:raise()
        end,
        {
            description = "(un)maximize horizontally",
            group = "client",
        }
    ),
    awful.key(
        {modkey, "Shift"}, "r",
        function(c)
            if not c.floating then
                c.floating = true
            end

            local scr = awful.screen.focused({client = true})
            local x = scr.geometry.x + (scr.geometry.width / 2 - 800)
            local y = scr.geometry.y + (scr.geometry.height / 2 - 450)
            c:geometry({
                x = x,
                y = y,
                width = 1600,
                height = 900,
            })
        end,
        {
            description = "resize to 1600x900",
            group = "client",
        }
    ),
    awful.key(
        {modkey}, "i",
        function(c)
            naughty.notify({
                title = c.name,
                text = "class: " .. c.class .. "\ninstance: " .. c.instance .. "\nrole: " .. (c.role or "<none>") .. "\ntype: " .. c.type,
            })
        end,
        {
            description = "dump client info",
            group = "client",
        }
    )
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = gears.table.join(globalkeys,
        -- View tag only.
        awful.key(
            {modkey}, "#" .. i + 9,
            function()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    tag:view_only()
                end
            end,
            {
                description = "view tag #"..i,
                group = "tag",
            }
        ),
        -- Toggle tag display.
        awful.key(
            {modkey, "Control"}, "#" .. i + 9,
            function()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    awful.tag.viewtoggle(tag)
                end
            end,
            {
                description = "toggle tag #" .. i,
                group = "tag"
            }
        ),
        -- Move client to tag.
        awful.key(
            {modkey, "Shift"}, "#" .. i + 9,
            function ()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:move_to_tag(tag)
                    end
                end
            end,
            {
                description = "move focused client to tag #"..i,
                group = "tag",
            }
        ),
        -- Toggle tag on focused client.
        awful.key(
            {modkey, "Control", "Shift"}, "#" .. i + 9,
            function ()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:toggle_tag(tag)
                    end
                end
            end,
            {
                description = "toggle focused client on tag #" .. i,
                group = "tag",
            }
        )
    )
end

local titlebar_positions = {"top", "bottom", "left", "right"}
local clientbuttons = gears.table.join(
    awful.button({}, 1, function(c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
    end),
    awful.button({modkey}, 1, function(c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.move(c)
    end),
    awful.button({modkey}, 3, function(c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.resize(c)
    end),
    awful.button({modkey, "Shift"}, 1, function(c)
        for _, pos in ipairs(titlebar_positions) do
            awful.titlebar.hide(c, pos)
        end
    end),
    awful.button({modkey, "Shift"}, 3, function(c)
        for _, pos in ipairs(titlebar_positions) do
            awful.titlebar.show(c, pos)
        end
    end)
)

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    {
        rule = {},
        properties = {
            border_width = 0, --beautiful.border_width,
            border_color = beautiful.border_normal,
            focus = awful.client.focus.filter,
            raise = true,
            keys = clientkeys,
            buttons = clientbuttons,
            screen = awful.screen.preferred,
            placement = awful.placement.no_overlap + awful.placement.no_offscreen,
        }
    },

    -- Floating clients.
    {
        rule_any = {
            instance = {
                "DTA",  -- Firefox addon DownThemAll.
                "copyq",  -- Includes session name in class.
                "pinentry",
            },
            class = {
                "Arandr",
                "Blueman-manager",
                "Gpick",
                "Kruler",
                "MessageWin",  -- kalarm.
                "Sxiv",
                "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
                "Wpa_gui",
                "veromix",
                "xtightvncviewer",
                "scrcpy",
            },

            -- Note that the name property shown in xprop might be set slightly after creation of the client
            -- and the name shown there might not match defined rules here.
            name = {
                "Event Tester",  -- xev.
            },
            role = {
                "AlarmWindow",  -- Thunderbird's calendar.
                "ConfigManager",  -- Thunderbird's about:config.
                "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
                "devtools",
                "toolbox",
            }
        },
        properties = {
            floating = true
        }
    },

    -- Add titlebars to normal clients and dialogs
    {
        rule_any = {
            type = {
                "normal",
                "dialog",
            }
        },
        properties = {
            titlebars_enabled = true
        },
    },

    -- steam windows float + no titlebar
    {
        rule = {
            instance = "Steam",
        },
        properties = {
            floating = true,
            titlebars_enabled = false,
        },
    },

    -- tg media preview fullscreen
    {
        rule = {
            name = "Media viewer",
            instance = "telegram-desktop",
        },
        properties = {
            titlebars_enabled = false,
            floating = true,
            fullscreen = true,
        },
    },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if
        awesome.startup
        and not c.size_hints.user_position
        and not c.size_hints.program_position
    then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

local win9x_l = {
    wibox.widget({
        widget = wibox.widget.separator,
        color = beautiful.win9x_outer_bright,
        orientation = "vertical",
        thickness = 1,
        forced_width = 1,
        border_width = 0,
    }),
    wibox.widget({
        widget = wibox.widget.separator,
        color = beautiful.win9x_bright,
        orientation = "vertical",
        thickness = 1,
        forced_width = 1,
        border_width = 0,
    }),
    wibox.widget({
        widget = wibox.widget.separator,
        color = beautiful.win9x_main,
        orientation = "vertical",
        thickness = 2,
        forced_width = 2,
        border_width = 0,
    }),
    forced_width = 4,
    layout = wibox.layout.fixed.horizontal,
}
local win9x_r = {
    wibox.widget({
        widget = wibox.widget.separator,
        color = beautiful.win9x_main,
        orientation = "vertical",
        thickness = 2,
        forced_width = 2,
        border_width = 0,
    }),
    wibox.widget({
        widget = wibox.widget.separator,
        color = beautiful.win9x_dark,
        orientation = "vertical",
        thickness = 1,
        forced_width = 1,
        border_width = 0,
    }),
    wibox.widget({
        widget = wibox.widget.separator,
        color = beautiful.win9x_outer_dark,
        orientation = "vertical",
        thickness = 1,
        forced_width = 1,
        border_width = 0,
    }),
    forced_width = 4,
    layout = wibox.layout.fixed.horizontal,
}
local win9x_bl = {
    {
        wibox.widget({
            widget = wibox.widget.separator,
            color = beautiful.win9x_outer_bright,
            orientation = "vertical",
            thickness = 1,
            forced_width = 1,
            border_width = 0,
        }),
        {
            {
                wibox.widget({
                    widget = wibox.widget.separator,
                    color = beautiful.win9x_bright,
                    orientation = "vertical",
                    thickness = 1,
                    forced_width = 1,
                    border_width = 0,
                }),
                wibox.widget({
                    widget = wibox.widget.separator,
                    color = beautiful.win9x_main,
                    orientation = "vertical",
                    thickness = 2,
                    forced_width = 2,
                    forced_height = 2,
                    border_width = 0,
                }),
                forced_height = 2,
                forced_width = 3,
                layout = wibox.layout.fixed.horizontal,
            },
            wibox.widget({
                widget = wibox.widget.separator,
                color = beautiful.win9x_dark,
                orientation = "horizontal",
                thickness = 1,
                forced_height = 1,
                border_width = 0,
            }),
            forced_height = 3,
            forced_width = 3,
            layout = wibox.layout.fixed.vertical,
        },
        forced_height = 3,
        forced_width = 4,
        layout = wibox.layout.fixed.horizontal,
    },
    wibox.widget({
        widget = wibox.widget.separator,
        color = beautiful.win9x_outer_dark,
        orientation = "horizontal",
        thickness = 1,
        forced_height = 1,
        border_width = 0,
    }),
    forced_height = 4,
    forced_width = 4,
    layout = wibox.layout.fixed.vertical,
}
local win9x_b = {
    wibox.widget({
        widget = wibox.widget.separator,
        color = beautiful.win9x_main,
        orientation = "horizontal",
        thickness = 2,
        forced_height = 2,
        border_width = 0,
    }),
    wibox.widget({
        widget = wibox.widget.separator,
        color = beautiful.win9x_dark,
        orientation = "horizontal",
        thickness = 1,
        forced_height = 1,
        border_width = 0,
    }),
    wibox.widget({
        widget = wibox.widget.separator,
        color = beautiful.win9x_outer_dark,
        orientation = "horizontal",
        thickness = 1,
        forced_height = 1,
        border_width = 0,
    }),
    forced_height = 4,
    layout = wibox.layout.fixed.vertical,
}
local win9x_br = {
    {
        {
            {
                wibox.widget({
                    widget = wibox.widget.separator,
                    color = beautiful.win9x_main,
                    orientation = "vertical",
                    thickness = 2,
                    forced_width = 2,
                    forced_height = 2,
                    border_width = 0,
                }),
                wibox.widget({
                    widget = wibox.widget.separator,
                    color = beautiful.win9x_dark,
                    orientation = "vertical",
                    thickness = 1,
                    forced_width = 1,
                    border_width = 0,
                }),
                forced_height = 2,
                forced_width = 3,
                layout = wibox.layout.fixed.horizontal,
            },
            wibox.widget({
                widget = wibox.widget.separator,
                color = beautiful.win9x_dark,
                orientation = "horizontal",
                thickness = 1,
                forced_height = 1,
                border_width = 0,
            }),
            forced_height = 3,
            forced_width = 3,
            layout = wibox.layout.fixed.vertical,
        },
        wibox.widget({
            widget = wibox.widget.separator,
            color = beautiful.win9x_outer_dark,
            orientation = "vertical",
            thickness = 1,
            forced_width = 1,
            border_width = 0,
        }),
        forced_height = 3,
        forced_width = 4,
        layout = wibox.layout.fixed.horizontal,
    },
    wibox.widget({
        widget = wibox.widget.separator,
        color = beautiful.win9x_outer_dark,
        orientation = "horizontal",
        thickness = 1,
        forced_height = 1,
        border_width = 0,
    }),
    forced_height = 4,
    forced_width = 4,
    layout = wibox.layout.fixed.vertical,
}
local win9x_tl = {
    wibox.widget({
        widget = wibox.widget.separator,
        color = beautiful.win9x_outer_bright,
        orientation = "horizontal",
        thickness = 1,
        forced_height = 1,
        border_width = 0,
    }),
    {
        wibox.widget({
            widget = wibox.widget.separator,
            color = beautiful.win9x_outer_bright,
            orientation = "vertical",
            thickness = 1,
            forced_width = 1,
            border_width = 0,
        }),
        {
            wibox.widget({
                widget = wibox.widget.separator,
                color = beautiful.win9x_bright,
                orientation = "horizontal",
                thickness = 1,
                forced_height = 1,
                border_width = 0,
            }),
            {
                wibox.widget({
                    widget = wibox.widget.separator,
                    color = beautiful.win9x_bright,
                    orientation = "vertical",
                    thickness = 1,
                    forced_width = 1,
                    border_width = 0,
                }),
                wibox.widget({
                    widget = wibox.widget.separator,
                    color = beautiful.win9x_main,
                    orientation = "vertical",
                    thickness = 2,
                    forced_width = 2,
                    forced_height = 2,
                    border_width = 0,
                }),
                forced_height = 2,
                forced_width = 3,
                layout = wibox.layout.fixed.horizontal,
            },
            forced_height = 3,
            forced_width = 3,
            layout = wibox.layout.fixed.vertical,
        },
        forced_height = 3,
        forced_width = 4,
        layout = wibox.layout.fixed.horizontal,
    },
    forced_height = 4,
    forced_width = 4,
    layout = wibox.layout.fixed.vertical,
}
local win9x_t = {
    wibox.widget({
        widget = wibox.widget.separator,
        color = beautiful.win9x_outer_bright,
        orientation = "horizontal",
        thickness = 1,
        forced_height = 1,
        border_width = 0,
    }),
    wibox.widget({
        widget = wibox.widget.separator,
        color = beautiful.win9x_bright,
        orientation = "horizontal",
        thickness = 1,
        forced_height = 1,
        border_width = 0,
    }),
    wibox.widget({
        widget = wibox.widget.separator,
        color = beautiful.win9x_main,
        orientation = "horizontal",
        thickness = 2,
        forced_height = 2,
        border_width = 0,
    }),
    forced_height = 4,
    layout = wibox.layout.fixed.vertical,
}
local win9x_tr = {
    {
        wibox.widget({
            widget = wibox.widget.separator,
            color = beautiful.win9x_outer_bright,
            orientation = "horizontal",
            thickness = 1,
            forced_height = 1,
            border_width = 0,
        }),
        {
            {
                wibox.widget({
                    widget = wibox.widget.separator,
                    color = beautiful.win9x_bright,
                    orientation = "horizontal",
                    thickness = 1,
                    forced_height = 1,
                    border_width = 0,
                }),
                wibox.widget({
                    widget = wibox.widget.separator,
                    color = beautiful.win9x_main,
                    orientation = "vertical",
                    thickness = 2,
                    forced_width = 2,
                    forced_height = 2,
                    border_width = 0,
                }),
                forced_height = 3,
                forced_width = 2,
                layout = wibox.layout.fixed.vertical,
            },
            wibox.widget({
                widget = wibox.widget.separator,
                color = beautiful.win9x_dark,
                orientation = "vertical",
                thickness = 1,
                forced_width = 1,
                border_width = 0,
            }),
            forced_height = 3,
            forced_width = 3,
            layout = wibox.layout.fixed.horizontal,
        },
        forced_height = 4,
        forced_width = 3,
        layout = wibox.layout.fixed.vertical,
    },
    wibox.widget({
        widget = wibox.widget.separator,
        color = beautiful.win9x_outer_dark,
        orientation = "vertical",
        thickness = 1,
        forced_width = 1,
        border_width = 0,
    }),
    forced_height = 4,
    forced_width = 4,
    layout = wibox.layout.fixed.horizontal,
}
local win9x_div = {
    wibox.widget({
        widget = wibox.widget.separator,
        color = beautiful.win9x_main,
        orientation = "horizontal",
        thickness = 2,
        forced_height = 2,
        border_width = 0,
    }),
    forced_height = 2,
    layout = wibox.layout.fixed.vertical,
}
local win9x_btn = {
    {
        {
            wibox.widget({
                widget = wibox.widget.separator,
                color = beautiful.win9x_bright,
                orientation = "vertical",
                thickness = 1,
                forced_width = 1,
                border_width = 0,
            }),
            {
                wibox.widget({
                    widget = wibox.widget.separator,
                    color = beautiful.win9x_bright,
                    orientation = "horizontal",
                    thickness = 1,
                    forced_height = 1,
                    border_width = 0,
                }),
                {
                    wibox.widget({
                        widget = wibox.widget.separator,
                        color = beautiful.win9x_outer_bright,
                        orientation = "vertical",
                        thickness = 1,
                        forced_width = 1,
                        border_width = 0,
                    }),
                    {
                        wibox.widget({
                            widget = wibox.widget.separator,
                            color = beautiful.win9x_outer_bright,
                            orientation = "horizontal",
                            thickness = 1,
                            forced_height = 1,
                            border_width = 0,
                        }),
                        wibox.widget({
                            widget = wibox.widget.separator,
                            color = beautiful.win9x_main,
                            orientation = "horizontal",
                            thickness = 10,
                            forced_height = 10,
                            border_width = 0,
                        }),
                        forced_height = 11,
                        forced_width = 12,
                        layout = wibox.layout.fixed.vertical,
                    },
                    wibox.widget({
                        widget = wibox.widget.separator,
                        color = beautiful.win9x_dark,
                        orientation = "vertical",
                        thickness = 1,
                        forced_width = 1,
                        border_width = 0,
                    }),
                    forced_height = 11,
                    forced_width = 14,
                    layout = wibox.layout.fixed.horizontal,
                },
                wibox.widget({
                    widget = wibox.widget.separator,
                    color = beautiful.win9x_dark,
                    orientation = "horizontal",
                    thickness = 1,
                    forced_height = 1,
                    border_width = 0,
                }),
                forced_height = 13,
                forced_width = 14,
                layout = wibox.layout.fixed.vertical,
            },
            wibox.widget({
                widget = wibox.widget.separator,
                color = beautiful.win9x_outer_dark,
                orientation = "vertical",
                thickness = 1,
                forced_width = 1,
                border_width = 0,
            }),
            forced_height = 13,
            forced_width = 16,
            layout = wibox.layout.fixed.horizontal,
        },
        wibox.widget({
            widget = wibox.widget.separator,
            color = beautiful.win9x_outer_dark,
            orientation = "horizontal",
            thickness = 1,
            forced_height = 1,
            border_width = 0,
        }),
        forced_height = 14,
        forced_width = 16,
        layout = wibox.layout.fixed.vertical,
    },
    forced_height = 16,
    forced_width = 16,
    top = 1,
    bottom = 1,
    layout = wibox.container.margin,
}

local function win9xButton(child)
    return {
        win9x_btn,
        child,
        layout = wibox.layout.stack,
    }
end

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = gears.table.join(
        awful.button({}, 1, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.move(c)
        end),
        awful.button({}, 3, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c, {
        position = "left",
        size = 4,
        bg_normal = "#00000000",
        bg_focus = "#00000000",
    }):setup(win9x_l)
    awful.titlebar(c, {
        position = "right",
        size = 4,
        bg_normal = "#00000000",
        bg_focus = "#00000000",
    }):setup(win9x_r)

    awful.titlebar(c, {
        position = "bottom",
        size = 4,
        bg_normal = "#00000000",
        bg_focus = "#00000000",
    }):setup({
        win9x_bl,
        win9x_b,
        win9x_br,
        layout = wibox.layout.align.horizontal,
    })

    local titlebar = awful.titlebar(c, {
        size = 24,
        fg_normal = beautiful.win9x_unfocus,
        fg_focus = beautiful.fg_focus,
        bg_normal = beautiful.win9x_outer_bright,
        bg_focus = beautiful.win9x_focus,
    })

    titlebar:setup({
        {
            win9x_tl,
            win9x_t,
            win9x_tr,
            forced_height = 4,
            layout = wibox.layout.align.horizontal,
        },
        {
            win9x_l,
            {
                {
                    { -- Left
                        {
                            awful.titlebar.widget.iconwidget(c),
                            left = 2,
                            right = 2,
                            top = 1,
                            bottom = 1,
                            layout = wibox.container.margin,
                        },
                        {
                            awful.titlebar.widget.titlewidget(c),
                            left = 2,
                            layout = wibox.container.margin,
                        },
                        buttons = buttons,
                        layout = wibox.layout.fixed.horizontal
                    },
                    { -- Middle
                        buttons = buttons,
                        layout = wibox.layout.flex.horizontal
                    },
                    { -- Right
                        {
                            {
                                win9xButton(awful.titlebar.widget.ontopbutton(c)),
                                win9xButton(awful.titlebar.widget.stickybutton(c)),
                                win9xButton(awful.titlebar.widget.floatingbutton(c)),
                                layout = wibox.layout.fixed.horizontal,
                            },
                            {
                                win9xButton(awful.titlebar.widget.minimizebutton(c)),
                                win9xButton(awful.titlebar.widget.maximizedbutton(c)),
                                layout = wibox.layout.fixed.horizontal,
                            },
                            win9xButton(awful.titlebar.widget.closebutton(c)),
                            spacing = 2,
                            layout = wibox.layout.fixed.horizontal
                        },
                        left = 2,
                        right = 2,
                        top = 1,
                        bottom = 1,
                        layout = wibox.container.margin,
                    },
                    forced_height = 18,
                    layout = wibox.layout.align.horizontal,
                },
                win9x_div,
                layout = wibox.layout.align.vertical,
            },
            win9x_r,
            layout = wibox.layout.align.horizontal,
        },
        layout = wibox.layout.align.vertical,
    })
end)

-- Enable sloppy focus, so that focus follows mouse.
--[[client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)--]]

client.connect_signal("focus", function(c)
    c.border_color = beautiful.border_focus
end)
client.connect_signal("unfocus", function(c)
    c.border_color = beautiful.border_normal
end)
-- }}}

--{{{ Autostart
-- taken from: https://gitlab.com/luna/til/-/blob/main/awesome/rc.lua
awful.spawn.with_line_callback(HOME .. "/.local/bin/autostart.sh", {
    stdout = function()
        --[[naughty.notify({
            title = "autostart",
            text = "pid: "..line.." already started",
            timeout = 7,
            position = "top_right"
        })--]]
    end,
    exit = function(reason, code)
        local data = {
            title = "autostart exited",
            text = reason .. ' ' .. code,
            timeout = 4,
        }

        if reason ~= "exit" and code ~= 0 then
            data.preset = naughty.config.presets.critical
        end

        naughty.notify(data)
    end
})
--}}}
