#!/bin/bash
if [ "`pgrep -x cmus`" == "" ]; then
  echo "<txt></txt>"
  exit 0
fi

status=$(cmus-remote -Q | awk '/status / {print $2}')
albumartist=$(cmus-remote -Q | awk '/tag albumartist / {$1="";$2="";print substr($0,3)}')
artist=$(cmus-remote -Q | awk '/tag artist / {$1="";$2="";print substr($0,3)}')
title=$(cmus-remote -Q | awk '/tag title / {$1="";$2="";print substr($0,3)}')
duration=$(cmus-remote -Q | awk '/duration/ {print $2}')
position=$(cmus-remote -Q | awk '/position/ {print $2}')

nowplaying=""

if [ "$albumartist" != "" ]; then
  nowplaying+="$albumartist"
else
  nowplaying+="$artist"
fi
nowplaying+=" - $title"

position_formatted=""
duration_formatted=""

if [ "$position" != "" ]; then
  if [ $position -ge 3600 ]; then
    position_formatted+=$(printf '%02d:' $(($position/3600)))
  fi

  position_formatted+=$(printf '%02d:%02d' $(($position%3600/60)) $(($position%60)))
fi

if [ "$duration" != "" ]; then
  if [ $duration -ge 3600 ]; then
    duration_formatted+=$(printf '%02d:' $(($duration/3600)))
  fi

  duration_formatted+=$(printf '%02d:%02d' $(($duration%3600/60)) $(($duration%60)))
fi

nowplaying+=" [$position_formatted/$duration_formatted]"

if [ "$status" == "paused" ]; then
  nowplaying+=" [paused]"
elif [ "$status" == "stopped" ]; then
  nowplaying+=" [stopped]"
fi

echo "<icon>mplayer</icon>"
echo "<txt> $nowplaying</txt>"
