#!/bin/sh

pkill -9 pipewire
pkill -9 pipewire-pulse
pkill -9 vmware-user-suid-wrapper
pkill -9 xmousepasteblock
pkill -9 xscreensaver

pipewire &
pipewire-pulse &
vmware-user-suid-wrapper &
xmousepasteblock &
xscreensaver &
