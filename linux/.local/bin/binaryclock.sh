#!/bin/bash

row0=("⠀" "⠈" "⠐" "⠘" "⠠" "⠨" "⠰" "⠸" "⢀" "⢈")
row1=("⠁" "⠉" "⠑" "⠙" "⠡" "⠩" "⠱" "⠹" "⢁" "⢉")
row2=("⠂" "⠊" "⠒" "⠚" "⠢" "⠪" "⠲" "⠺" "⢂" "⢊")
row3=("⠃" "⠋" "⠓" "⠛" "⠣" "⠫" "⠳" "⠻" "⢃" "⢋")
row4=("⠄" "⠌" "⠔" "⠜" "⠤" "⠬" "⠴" "⠼" "⢄" "⢌")
row5=("⠅" "⠍" "⠕" "⠝" "⠥" "⠭" "⠵" "⠽" "⢅" "⢍")
chars=("${row0[@]}" "${row1[@]}" "${row2[@]}" "${row3[@]}" "${row4[@]}" "${row5[@]}")

hour=$(date +"%H")
minute=$(date +"%M")
second=$(date +"%S")
[[ $hour = 0* ]] && hour=${hour:1:1}
[[ $minute = 0* ]] && minute=${minute:1:1}
[[ $second = 0* ]] && second=${second:1:1}

echo "${chars[$hour]} ${chars[$minute]} ${chars[$second]}"