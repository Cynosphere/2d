#!/bin/sh

COLORSCHEME="tokyonight"

run() {
  if ! pgrep "$1" ;
  then
    notify-send "autostart: starting $1"
    "$@" &
  else
    notify-send "autostart: $1 already running"
  fi
}

notify-send "begin autostart"

xrdb ~/.config/xrdb/main.xrdb
xrdb -merge "~/.config/xrdb/colors/$COLORSCHEME.xrdb"
xmodmap -e "keysym Menu = Multi_key"

run pipewire

run picom --config ~/.config/picom.conf
run xscreensaver -no-splash

#run xmousepasteblock
run xbindkeys -p

run flameshot
run gpick

run radeon-profile

if ! pgrep "nicotine" ; then
  notify-send "autostart: starting nicotine"
  gtk3-nocsd nicotine &
else
  notify-send "autostart: nicotine already running"
fi
