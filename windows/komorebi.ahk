#SingleInstance Force

; You can generate a fresh version of this file with "komorebic ahk-library"
#Include %A_ScriptDir%\komorebic.lib.ahk
; https://github.com/LGUG2Z/komorebi/#generating-common-application-specific-configurations
#Include %A_ScriptDir%\komorebi.generated.ahk

;;;; Settings ;;;;
WindowHidingBehaviour("minimize")

WatchConfiguration("enable")

EnsureWorkspaces(0, 9)

InvisibleBorders(7, 0, 14, 7)

MouseFollowsFocus("disable")

WorkspaceName(0, 0, "I")
WorkspaceName(0, 1, "II")
WorkspaceName(0, 2, "III")
WorkspaceName(0, 3, "IV")
WorkspaceName(0, 4, "V")
WorkspaceName(0, 5, "VI")
WorkspaceName(0, 6, "VII")
WorkspaceName(0, 7, "VIII")
WorkspaceName(0, 8, "IX")

WorkspacePadding(0, 0, 0)
WorkspacePadding(0, 1, 0)
WorkspacePadding(0, 2, 0)
WorkspacePadding(0, 3, 0)
WorkspacePadding(0, 4, 0)
WorkspacePadding(0, 5, 0)
WorkspacePadding(0, 6, 0)
WorkspacePadding(0, 7, 0)
WorkspacePadding(0, 8, 0)

ContainerPadding(0, 0, 6)
ContainerPadding(0, 1, 6)
ContainerPadding(0, 2, 6)
ContainerPadding(0, 3, 6)
ContainerPadding(0, 4, 6)
ContainerPadding(0, 5, 6)
ContainerPadding(0, 6, 6)
ContainerPadding(0, 7, 6)
ContainerPadding(0, 8, 6)

;;;; Rules ;;;;
FloatRule("exe", "steam.exe") ; personal preference
FloatRule("exe", "steamwebhelper.exe") ; personal preference
FloatRule("exe", "scrcpy.exe") ; personal preference
FloatRule("exe", "vmware.exe") ; TODO: pr?

FloatRule("title", "SplashScreenGuard") ; TODO: pr

FloatRule("class", "TFormHexView") ; TODO: pr
FloatRule("class", "WindowsForms10.Window.8.app.0.2bf8098_r7_ad1")

ManageRule("title", " - MusicBee")

IdentifyTrayApplication("exe", "thunderbird.exe") ; TODO: pr? doesnt work well if "minimize to tray" is enabled
IdentifyTrayApplication("exe", "foobar2000.exe")

IdentifyBorderOverflowApplication("exe", "MusicBee.exe") ; TODO: pr? also need to figure out how to make miniplayer popout not steal focus
IdentifyBorderOverflowApplication("exe", "vivaldi.exe") ; TODO: pr
IdentifyBorderOverflowApplication("exe", "mixxx.exe") ; TODO: pr

;;;; Finalize config ;;;;
CompleteConfiguration()

;;;; Keys ;;;;
#!q::
    Stop()
return
#!r::
    ReloadConfiguration()
return

#+h::
Move("left")
return

#+j::
Move("down")
return

#+k::
Move("up")
return

#+l::
Move("right")
return

#1::
    FocusWorkspace(0)
return
#2::
    FocusWorkspace(1)
return
#3::
    FocusWorkspace(2)
return
#4::
    FocusWorkspace(3)
return
#5::
    FocusWorkspace(4)
return
#6::
    FocusWorkspace(5)
return
#7::
    FocusWorkspace(6)
return
#8::
    FocusWorkspace(7)
return
#9::
    FocusWorkspace(8)
return

#+1::
    SendToWorkspace(0)
return
#+2::
    SendToWorkspace(1)
return
#+3::
    SendToWorkspace(2)
return
#+4::
    SendToWorkspace(3)
return
#+5::
    SendToWorkspace(4)
return
#+6::
    SendToWorkspace(5)
return
#+7::
    SendToWorkspace(6)
return
#+8::
    SendToWorkspace(7)
return
#+9::
    SendToWorkspace(8)
return

#f::
    ToggleFloat()
return

#t::
    Manage()
return
#+t::
    Unmanage()
return

#Enter::
    Run, wezterm-gui start
return
