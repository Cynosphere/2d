" {{{ plugins
call plug#begin(stdpath('data') . '/plugged')

" misc
Plug 'nvim-lua/plenary.nvim'
Plug 'tpope/vim-sensible'
Plug 'itchyny/vim-gitbranch'
Plug 'andweeb/presence.nvim'
Plug 'ntpeters/vim-better-whitespace'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'mg979/vim-visual-multi'
Plug 'rhysd/committia.vim'
Plug 'tpope/vim-sleuth'
Plug 'm-demare/hlargs.nvim'
Plug 'nvim-treesitter/nvim-treesitter'
Plug 'windwp/nvim-ts-autotag'
Plug 'mileszs/ack.vim'

" interface
Plug 'romgrk/barbar.nvim'
Plug 'lukas-reineke/indent-blankline.nvim'
Plug 'folke/lsp-colors.nvim'
Plug 'nacro90/numb.nvim'
Plug 'xiyaowong/nvim-cursorword'
Plug 'kevinhwang91/nvim-hlslens'

" language specific
Plug 'sheerun/vim-polyglot'
Plug 'dense-analysis/ale'
Plug 'neovim/nvim-lspconfig'

" autocomplete
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/cmp-buffer'
Plug 'dmitmel/cmp-cmdline-history'
Plug 'hrsh7th/cmp-nvim-lsp-signature-help'
Plug 'hrsh7th/cmp-path'

Plug 'hrsh7th/cmp-vsnip'
Plug 'hrsh7th/vim-vsnip'

call plug#end()
" }}}

" {{{ options
set notermguicolors
colorscheme lena

set fileformat=unix
set fileformats=unix,dos

set number
set cul

set tabstop=2
set shiftwidth=2
set expandtab

set mouse+=nia

au WinEnter * set cul
au WinLeave * set nocul

let g:better_whitespace_enabled = 1

let bufferline = get(g:, 'bufferline', {})
let bufferline.icons = v:false
let bufferline.icon_separator_active = '▎'
let bufferline.icon_separator_inactive = '▎'
let bufferline.icon_close_tab = 'x'
let bufferline.icon_close_tab_modified = '●'
let bufferline.icon_pinned = '▲'

let g:presence_buttons = 0

let g:ale_linters = {
\  'javascript': ['eslint'],
\  'typescript': ['eslint', 'tsserver'],
\  'rust': ['rust-analyzer'],
\}
let g:ale_fixers = {
\  '*': ['trim_whitespace'],
\  'vim': [],
\  'javascript': ['prettier', 'eslint'],
\  'typescript': ['prettier', 'eslint'],
\  'json': ['prettier'],
\  'css': ['prettier'],
\  'rust': ['rustfmt'],
\}
let g:ale_fix_on_save = 1

set fillchars+=vert:▎

set completeopt=menu,menuone,noselect

let g:ackprg = 'ag --vimgrep'
" }}}

" {{{ scripts
lua << EOF
vim.cmd("highlight IndentBlanklineIndent1 ctermfg=11 cterm=nocombine")
vim.cmd("highlight IndentBlanklineIndent2 ctermfg=10 cterm=nocombine")
vim.cmd("highlight IndentBlanklineIndent3 ctermfg=13 cterm=nocombine")
vim.cmd("highlight IndentBlanklineIndent4 ctermfg=12 cterm=nocombine")

vim.opt.list = true
vim.opt.listchars:append("space:⋅")
vim.opt.listchars:append("eol:↴")

require("indent_blankline").setup(
  {
    char = "▎",
    space_char_blankline = " ",
    char_blankline = " ",
    char_highlight_list = {
      "IndentBlanklineIndent1",
      "IndentBlanklineIndent2",
      "IndentBlanklineIndent3",
      "IndentBlanklineIndent4",
    },
    show_current_context = true,
    show_current_context_start = true,
  }
)
EOF

:lua require("numb").setup()
:lua require("nvim-ts-autotag").setup()

lua << EOF
require("hlslens").setup(
  {
    calm_down = true,
    nearest_only = true,
    nearest_float_when = "auto",
  }
)
EOF

lua <<EOF
local cmp = require("cmp")

local has_words_before = function()
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

local feedkey = function(key, mode)
  vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes(key, true, true, true), mode, true)
end

local mapping = {
  ["<C-Space>"] = cmp.mapping(cmp.mapping.complete(), {"i", "s", "c"}),
  ["<CR>"] = cmp.mapping(function(fallback)
    local entry = cmp.get_selected_entry()
    if entry then
      cmp.confirm({behavior = cmp.ConfirmBehavior.Insert, select = true})
    else
      fallback()
    end
  end, {"i", "s", "c"}),
  ["<C-f>"] = cmp.mapping(function(fallback)
    -- This little snippet will confirm with tab, and if no entry is selected, will confirm the first item
    if cmp.visible() then
      local entry = cmp.get_selected_entry()
      if not entry then
        cmp.select_next_item({ behavior = cmp.SelectBehavior.Select })
      else
        cmp.confirm()
      end
    else
      fallback()
    end
  end, {"i", "s", "c"}),
  ["<Tab>"] = cmp.mapping(cmp.mapping.abort(), {"i", "s", "c"}),
  ["<Esc>"] = cmp.mapping(cmp.mapping.abort(), {"i", "s", "c"}),
  ["<Down>"] = cmp.mapping(cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }), {"i", "s", "c"}),
  ["<Up>"] = cmp.mapping(cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Select }), {"i", "s", "c"}),
}

cmp.setup(
  {
    snippet = {
      expand = function(args)
        vim.fn["vsnip#anonymous"](args.body)
      end,
    },
    mapping = cmp.mapping.preset.insert(mapping),
    sources = cmp.config.sources(
      {
        {name = "nvim_lsp", priority = 1},
        {name = "vsnip"},
      },
      {
        {name = "buffer"},
	{name = "path"},
      }
    ),
  }
)

cmp.setup.cmdline(
  ":",
  {
    sources = {
      {name = "cmdline", group_index = 1},
      {name = "cmdline_history", group_index = 2},
    },
    mapping = cmp.mapping.preset.cmdline(mapping),
  }
)

cmp.setup.cmdline(
  {"/", "?"},
  {
    sources = {
      {name = "buffer"}
    },
    mapping = cmp.mapping.preset.cmdline(mapping),
  }
)

local lspconfig = require("lspconfig")
local capabilities = require("cmp_nvim_lsp").default_capabilities(vim.lsp.protocol.make_client_capabilities())

local servers = {"eslint", "tsserver", "rust_analyzer"}
for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup(
    {
      capabilities = capabilities,
    }
  )
end
EOF
" }}}

" {{{ keys
tnoremap <Esc> <C-\><C-n>

inoremap <silent> <C-f> <C-i>
cnoremap <silent> <C-f> <C-i>
map! <silent> <Tab> <Esc>
cnoremap <silent> <Tab> <C-c>

nnoremap <silent> <S-Up> :m-2<CR>
nnoremap <silent> <S-Down> :m+<CR>
inoremap <silent> <S-Up> <Esc>:m-2<CR>
inoremap <silent> <S-Down> <Esc>:m+<CR>

"nnoremap <silent> <C-b> :NERDTreeToggle<CR>
"nnoremap <silent> <C-t> :SymbolsOutline<CR>
":lua require'bufferline.state'.set_offset(require'nvim-tree.view'.win_open() and 30 or 0)<CR>

"inoremap <expr> <C-f> pumvisible() ? "\<C-n>" : "\<Tab>"
"inoremap <expr> <C-S-f> pumvisible() ? "\<C-p>": "\<S-Tab>"

" barbar keys
" Move to previous/next
nnoremap <silent>    <A-,> :BufferPrevious<CR>
nnoremap <silent>    <A-.> :BufferNext<CR>
" Re-order to previous/next
nnoremap <silent>    <A-<> :BufferMovePrevious<CR>
nnoremap <silent>    <A->> :BufferMoveNext<CR>
" Goto buffer in position...
nnoremap <silent>    <A-1> :BufferGoto 1<CR>
nnoremap <silent>    <A-2> :BufferGoto 2<CR>
nnoremap <silent>    <A-3> :BufferGoto 3<CR>
nnoremap <silent>    <A-4> :BufferGoto 4<CR>
nnoremap <silent>    <A-5> :BufferGoto 5<CR>
nnoremap <silent>    <A-6> :BufferGoto 6<CR>
nnoremap <silent>    <A-7> :BufferGoto 7<CR>
nnoremap <silent>    <A-8> :BufferGoto 8<CR>
nnoremap <silent>    <A-9> :BufferLast<CR>
" Pin/unpin buffer
nnoremap <silent>    <A-p> :BufferPin<CR>
" Close buffer
nnoremap <silent>    <A-c> :BufferClose<CR>

" }}}

" {{{ colors
hi Normal ctermbg=NONE
hi LineNr ctermfg=7
hi CursorLineNr ctermfg=1 ctermbg=0
hi CursorLine cterm=NONE ctermfg=NONE ctermbg=0
hi StatusLine ctermfg=2 ctermbg=8 cterm=NONE
hi StatusLineNC ctermfg=7 ctermbg=0 cterm=NONE
hi VertSplit ctermbg=0 ctermfg=0
hi ExtraWhitespace ctermbg=1
hi SignColumn ctermbg=8

hi BufferCurrent ctermfg=5 ctermbg=NONE
hi BufferCurrentIndex ctermfg=5 ctermbg=NONE
hi BufferCurrentMod ctermfg=9 ctermbg=NONE
hi BufferCurrentSign ctermfg=5 ctermbg=NONE
hi BufferCurrentTarget ctermfg=5 ctermbg=NONE
hi BufferVisible ctermfg=7 ctermbg=NONE
hi BufferVisibleIndex ctermfg=7 ctermbg=NONE
hi BufferVisibleMod ctermfg=9 ctermbg=NONE
hi BufferVisibleSign ctermfg=7 ctermbg=NONE
hi BufferVisibleTarget ctermfg=7 ctermbg=NONE
hi BufferInactive ctermfg=7 ctermbg=0
hi BufferInactiveIndex ctermfg=5 ctermbg=0
hi BufferInactiveMod ctermfg=1 ctermbg=0
hi BufferInactiveSign ctermfg=10 ctermbg=0
hi BufferInactiveTarget ctermfg=11 ctermbg=0
hi BufferTabpages ctermfg=7 ctermbg=0
hi BufferTabpageFill ctermfg=7 ctermbg=0
hi BufferOffset ctermfg=0 ctermbg=0

hi! link SignColumn LineNr

hi LspDiagnosticsDefaultHint ctermfg=6
hi LspDiagnosticsDefaultError ctermfg=1
hi LspDiagnosticsDefaultWarning ctermfg=3
hi LspDiagnosticsDefaultInformation ctermfg=7

hi ALEErrorSign ctermfg=1
hi ALEWarningSign ctermfg=3

hi StatusLineMode ctermbg=1 ctermfg=0
hi StatusLineGitBranch ctermbg=5 ctermfg=0
hi StatusLineChar ctermbg=4 ctermfg=0
hi StatusLineFormat ctermbg=3 ctermfg=0
hi StatusLineFileType ctermbg=7 ctermfg=0
" }}}

" {{{ statusline
function! GetMode()
  let l:m = mode(1)

  if l:m ==# "i"
    let l:mode = "INS"
  elseif l:m ==# "c"
    let l:mode = "CMD"
  elseif l:m[0] ==# "R"
    let l:mode = "REP"
  elseif l:m ==# "Rv"
    let l:mode = "REP"
  elseif l:m =~# '\v(v|V| |s|S| )'
    let l:mode = "VIS"
  else
    return ""
  endif

  return '  '.l:mode.' '
endfunction

function! GitBranch()
  let l:branch = gitbranch#name()
  return strlen(l:branch) ? '  ' . l:branch . ' ' : ''
endfunction

set statusline=
set statusline+=%#StatusLineMode#
set statusline+=%{GetMode()}
set statusline+=%#StatusLineGitBranch#
set statusline+=%{GitBranch()}
set statusline+=%*
set statusline+=\ %f
set statusline+=\ %m%r
set statusline+=%=
set statusline+=%#StatusLineChar#
set statusline+=\ %l,\ %c\ 
set statusline+=%#StatusLineFormat#
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}\ %{&fileformat}\ 
set statusline+=%#StatusLineFileType#
set statusline+=\ %{&filetype}\ 
" }}}
