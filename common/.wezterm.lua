local wezterm = require("wezterm");
local config = {
  font_size = 9.0,
  freetype_load_target = "Mono",
  use_cap_height_to_scale_fallback_fonts = true,
  bold_brightens_ansi_colors = true,
  color_scheme = "Amora Focus",
  color_schemes = {
    everforest = {
      foreground = "#d3c6aa",
      background = "#2b3339",

      cursor_fg = "#3a454a",
      cursor_bg = "#d3c6aa",
      cursor_border = "#d3c6aa",

      selection_fg = "#9da9a0",
      selection_bg = "#7a8478",

      ansi = {
        "#323C41",
        "#e67e80",
        "#a7c080",
        "#dbbc7f",
        "#7fbbb3",
        "#d699b6",
        "#83c092",
        "#d3c6aa",
      },
      brights = {
        "#3A454A",
        "#f85552",
        "#8da101",
        "#dfa000",
        "#3a94c5",
        "#df69ba",
        "#35a77c",
        "#9da9a0",
      },

      tab_bar = {
        background = "#1e2327",
        active_tab = {
          bg_color = "#2b3339",
          fg_color = "#d3c6aa",
        },
        inactive_tab = {
          bg_color = "#1e2327",
          fg_color = "#9da9a0",
        },
        inactive_tab_hover = {
          bg_color = "#1e2327",
          fg_color = "#d3c6aa",
          italic = false,
        },
        new_tab = {
          bg_color = "#1e2327",
          fg_color = "#9da9a0",
          italic = false,
        },
        new_tab_hover = {
          bg_color = "#1e2327",
          fg_color = "#d3c6aa",
          italic = false,
        },
      },
    },
    ["Tokyo Night"] = {
      background = "#1a1b26",
      foreground = "#a9b1d6",

      cursor_fg = "#444b6a",
      cursor_bg = "#9da9a0",
      cursor_border = "#9da9a0",

      selection_fg = "#787c99",
      selection_bg = "#444b6a",

      ansi = {
        "#32344a",
        "#f7768e",
        "#9ece6a",
        "#e0af68",
        "#7aa2f7",
        "#ad8ee6",
        "#449dab",
        "#787c99",
      },

      brights = {
        "#444b6a",
        "#ff7a93",
        "#b9f27c",
        "#ff9e64",
        "#7da6ff",
        "#bb9af7",
        "#0db9d7",
        "#acb0d0",
      },

      tab_bar = {
        background = "#16161e",
        active_tab = {
          bg_color = "#1a1b26",
          fg_color = "#a9b1d6",
        },
        inactive_tab = {
          bg_color = "#16161e",
          fg_color = "#787c99",
        },
        inactive_tab_hover = {
          bg_color = "#16161e",
          fg_color = "#a9b1d6",
          italic = false,
        },
        new_tab = {
          bg_color = "#16161e",
          fg_color = "#9da9a0",
          italic = false,
        },
        new_tab_hover = {
          bg_color = "#16161e",
          fg_color = "#a9b1d6",
          italic = false,
        },
      },
    },
    ["Amora Focus"] = {
      background = "#1a1a1a",
      foreground = "#dedbeb",

      cursor_fg = "#2d2d2d",
      cursor_bg = "#5c5c5c",
      cursor_border = "#5c5c5c",

      selection_fg = "#dedbeb",
      selection_bg = "#2d2d2d",

      ansi = {
        "#2d2d2d",
        "#e83f80",
        "#a2baa8",
        "#eacac0",
        "#9985d1",
        "#e68ac1",
        "#aabae7",
        "#dedbeb",
      },

      brights = {
        "#383838",
        "#f55d8f",
        "#bfd1c3",
        "#f0ddd8",
        "#b4a4de",
        "#edabd2",
        "#c4d1f5",
        "#edebf7",
      },

      tab_bar = {
        background = "#141414",
        active_tab = {
          bg_color = "#1a1a1a",
          fg_color = "#dedbeb",
        },
        inactive_tab = {
          bg_color = "#171717",
          fg_color = "#edebf7",
        },
        inactive_tab_hover = {
          bg_color = "#171717",
          fg_color = "#dedbeb",
          italic = false,
        },
        new_tab = {
          bg_color = "#141414",
          fg_color = "#edebf7",
          italic = false,
        },
        new_tab_hover = {
          bg_color = "#171717",
          fg_color = "#dedbeb",
          italic = false,
        },
      },
    },
    Discord = {
      background = "#36393f",
      foreground = "#dcddde",

      cursor_fg = "#202225",
      cursor_bg = "#b9bbbe",
      cursor_border = "#b9bbbe",

      selection_fg = "#dcddde",
      selection_bg = "#202225",

      ansi = {
        "#202225",
        "#ed4245",
        "#46c46d",
        "#faa61a",
        "#00b0f4",
        "#b377f3",
        "#45ddc0",
        "#a3a6aa",
      },

      brights = {
        "#4f545c",
        "#f1686a",
        "#57f287",
        "#fbb848",
        "#33c0f6",
        "#d09aff",
        "#86dcc5",
        "#b9bbbe",
      },

      tab_bar = {
        background = "#292b2f",
        active_tab = {
          bg_color = "#36393f",
          fg_color = "#dcddde",
        },
        inactive_tab = {
          bg_color = "#2f3136",
          fg_color = "#b9bbbe",
        },
        inactive_tab_hover = {
          bg_color = "#2f3136",
          fg_color = "#dcddde",
          italic = false,
        },
        new_tab = {
          bg_color = "#292b2f",
          fg_color = "#b9bbbe",
          italic = false,
        },
        new_tab_hover = {
          bg_color = "#2f3136",
          fg_color = "#dcddde",
          italic = false,
        },
      },
    },
  },
  window_padding = {
    left = 4,
    right = 4,
    top = 4,
    bottom = 4,
  },
  use_fancy_tab_bar = false,
  tab_bar_at_bottom = true,
  launch_menu = {},
  keys = {
    -- disable search (ctrl f = tab)
    {key = "f", mods = "CTRL|SHIFT", action = "DisableDefaultAssignment"},
    -- launcher
    {key = "Space", mods = "CTRL|SHIFT", action = "ShowLauncher"},
  },
  mouse_bindings = {
    -- Change the default selection behavior so that it only selects text,
    -- but doesn't copy it to a clipboard or open hyperlinks.
    {
      event={Up={streak=1, button="Left"}},
      mods="NONE",
      action=wezterm.action{ExtendSelectionToMouseCursor="Cell"}
    },
    -- Don't automatically copy the selection to the clipboard
    -- when double clicking a word
    {
      event={Up={streak=2, button="Left"}},
      mods="NONE",
      action="Nop",
    },
    -- Ctrl-click will open the link under the mouse cursor
    {
      event={Up={streak=1, button="Left"}},
      mods="CTRL",
      action="OpenLinkAtMouseCursor",
    },
  },
}

if wezterm.target_triple == "x86_64-pc-windows-msvc" then
  config.default_prog = {"D:/Programs/git-sdk-64/usr/bin/zsh", "-i"}
  config.default_cwd = "D:/Downloads"
  config.font = wezterm.font_with_fallback({
    "Terminus (TTF)",
    "Unifont Windows",
    "SijiNGWindows",
    "Noto Emoji",
  })
  config.unix_domains = {
    {
      name = "unix",
    },
  }
  -- config.default_gui_startup_args = {"connect", "unix"}

  table.insert(config.launch_menu, {
    label = "MSYS2",
    args = {
      "C:/msys64/msys2_shell.cmd",
      "-no-start","-defterm","-msys2",
    },
  })
  table.insert(config.launch_menu, {
    label = "MinGW64",
    args = {
      "C:/msys64/msys2_shell.cmd",
      "-no-start","-defterm","-mingw64",
    },
  })
  table.insert(config.launch_menu, {
    label = "cygwin",
    args = {
      "D:/Programs/cygwin/bin/bash",
      "--login",
    },
  })
else
  local function fallback(font)
    return wezterm.font_with_fallback({
      font,
      "Fixed",
      "Terminus (TTF)",
      "Sazanami Gothic",
      "Unifont",
      "Noto Emoji",
    })
  end
  config.font = fallback("tewi")
  config.font_rules = {
    {
      intensity = "Bold",
      font = fallback({family = "tewi", style = "Normal"}),
    },
    {
      italic = true,
      font = fallback({family = "orp", style = "Italic"}),
    },
    {
      intensity = "Bold",
      italic = true,
      font = fallback({family = "orp", style = "Italic"}),
    }
  }
  --[[config.font = wezterm.font_with_fallback({
    {family = "tewi", style = "Normal"},
    {family = "tewi", style = "Normal", weight = "Bold"},
    {family = "orp", style = "Italic"},
    {family = "orp", style = "Italic", weight = "Bold"},
    "Terminus (TTF)",
    "Unifont",
    "Noto Emoji",
  })--]]
end

return config
